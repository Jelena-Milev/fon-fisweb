<%-- 
    Document   : all
    Created on : May 5, 2020, 7:01:55 PM
    Author     : jeca
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Department All</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">         
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>  
    </head>
    <body>
        <%@include file="../templates/header.jsp"%>
        <div class="container"> 
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <h1  style="padding: 20px 20px;">Departments</h1>
                    <c:if test="${not empty requestScope.messageSuccess}">
                        <div class="alert alert-success">${requestScope.messageSuccess}</div>
                    </c:if>
                    <c:if test="${not empty requestScope.messageError}">
                        <div class="alert alert-danger">${requestScope.messageError}</div>
                    </c:if>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Shortname</th>
                                <th scope="col">Edit</th>
                                <th scope="col">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="department" items="${applicationScope.departments}">
                            <form action="/fis_web/app/department/change?id=${department.id}" method="post">
                                <tr>
                                    <td>
                                        <input type="text" name="name" value="${department.name}" class="form-control-plaintext" readonly/>
                                    </td>
                                    <td>
                                        <input type="text" name="shortname" value="${department.shortname}" class="form-control-plaintext" readonly/>
                                    </td>
                                    <td><button type="submit" class="btn btn-info">Edit</button></td>
                                    <td><a href="/fis_web/app/department/delete?id=${department.id}" class="badge badge-danger" style="padding: 12px 4px 12px 4px; font-size: 16px; font-weight: normal;">Delete</a></td>
                                </tr>
                            </form>
                        </c:forEach>    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
