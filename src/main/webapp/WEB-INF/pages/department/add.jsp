<%-- 
    Document   : add
    Created on : May 5, 2020, 4:36:52 PM
    Author     : jeca
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Department</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">         
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>  
    </head>
    <body>
        <%@include file="../templates/header.jsp" %>
        <div class="container"> 
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <h1 style="padding: 20px;">Add new department</h1>
                    <c:if test="${not empty requestScope.messageAddDepartment}">
                        <div class="alert alert-success">${requestScope.messageAddDepartment}</div>
                    </c:if>
                    <form action="/fis_web/app/department/save" method="POST">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="shortname" class="col-sm-2 col-form-label">Shortname</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="shortname" name="shortname" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-info">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
