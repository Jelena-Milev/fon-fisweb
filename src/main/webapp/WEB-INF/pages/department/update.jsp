<%-- 
    Document   : update
    Created on : May 6, 2020, 12:22:52 AM
    Author     : jeca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update department</title>
    </head>
    <body>
        <%@include file="../templates/header.jsp" %>
        <div class="container"> 
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <h1 style="padding: 20px;">Update department</h1>
                    <form action="/fis_web/app/department/update?id=${requestScope.department.id}" method="post">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" id="name" name="name" value="${requestScope.department.name}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="shortname" class="col-sm-2 col-form-label">Shortname</label>
                            <div class="col-sm-10">
                                <input type="text" id="shortname" name="shortname" value="${requestScope.department.shortname}" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-info">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
