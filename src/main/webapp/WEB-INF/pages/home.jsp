<%-- 
    Document   : home
    Created on : May 4, 2020, 1:18:24 AM
    Author     : jeca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
    </head>
    <%@include file="templates/header.jsp" %>
    <body>
        <h1>Welcome Home ${sessionScope.current_user.name}!</h1>
        
    </body>
</html>
