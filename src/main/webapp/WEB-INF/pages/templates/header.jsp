<%-- 
    Document   : header
    Created on : May 6, 2020, 12:59:42 AM
    Author     : jeca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">         
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>  
    </head>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="d-flex flex-grow-1">
        <span class="w-100 d-lg-none d-block"><!-- hidden spacer to center brand on mobile --></span>
        <a class="navbar-brand" href="#">Katedre</a>
        <div class="w-100 text-right">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
    <div class="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
        <ul class="navbar-nav ml-auto flex-nowrap">
            <li class="nav-item">
                <a class="nav-link" href="/fis_web/app/department/add">Add department</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/fis_web/app/department/all">All departments</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"href="/fis_web/app/logout">Logout</a>
            </li>
        </ul>
    </div>
</nav>
</html>
