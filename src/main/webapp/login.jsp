<%-- 
    Document   : login
    Created on : May 4, 2020, 12:54:35 AM
    Author     : jeca
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">         
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>  
    </head>
    <body>
        <div class="container"> 
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <h1 style="padding: 20px;">Login</h1>
                    <c:if test="${not empty requestScope.message}">
                        <div class="alert alert-danger">${requestScope.message}</div>
                    </c:if>

                    <form action="/fis_web/app/login" method="post">
                        <div class="form-group">
                            <label for="id">Email: </label>
                            <input type="email" id="email" name="email" value="" class="form-control" required="true">
                        </div>
                        <div class="form-group">
                            <label for="password">Password: </label>
                            <input type="password" id="password" name="password" value="" class="form-control" required="true">
                        </div>
                        <input type="submit" id="login" value="Login" class="btn btn-info"/>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
