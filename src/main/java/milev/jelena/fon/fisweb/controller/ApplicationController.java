/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.controller;

import javax.servlet.http.HttpServletRequest;
import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.action.factory.ActionFactory;
import milev.jelena.fon.fisweb.action.impl.LoginAction;
import milev.jelena.fon.fisweb.action.impl.LogoutAction;
import milev.jelena.fon.fisweb.constants.PageViewConstants;

/**
 *
 * @author jeca
 */
public class ApplicationController {

    public String resolveRequest(String pathInfo, HttpServletRequest request) {
        String view = PageViewConstants.VIEW_DEFAULT_ERROR;
        AbstractAction action = ActionFactory.createActionFactory(pathInfo);
        if (action != null) {
            view = action.execute(request);
        }
        return view;
    }

}
