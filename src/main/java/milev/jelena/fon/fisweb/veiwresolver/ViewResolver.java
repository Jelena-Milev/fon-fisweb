/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.veiwresolver;

import java.util.HashMap;
import java.util.Map;
import milev.jelena.fon.fisweb.constants.PageViewConstants;

/**
 *
 * @author jeca
 */
public class ViewResolver {

    private final Map<String, String> viewResourceMap;
    
    public ViewResolver() {
        viewResourceMap = new HashMap(){{
            put(PageViewConstants.VIEW_LOGIN, PageViewConstants.PAGE_LOGIN);
            put(PageViewConstants.VIEW_HOME, PageViewConstants.PAGE_HOME);
            put(PageViewConstants.VIEW_DEFAULT_ERROR, PageViewConstants.PAGE_DEFAULT_ERROR);
            put(PageViewConstants.VIEW_ADD_DEPARTMENT, PageViewConstants.PAGE_ADD_DEPARTMENT);
            put(PageViewConstants.VIEW_ALL_DEPARTMENT, PageViewConstants.PAGE_ALL_DEPARTMENT);
            put(PageViewConstants.VIEW_UPDATE_DEPARTMENT, PageViewConstants.PAGE_UPDATE_DEPARTMENT);
        }};
    }

    public String resolveView(String view) {
        return viewResourceMap.get(view);
    }
}
