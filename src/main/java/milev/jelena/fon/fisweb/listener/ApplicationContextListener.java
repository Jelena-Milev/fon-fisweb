/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.listener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import milev.jelena.fon.fisweb.model.Department;
import milev.jelena.fon.fisweb.model.User;

/**
 * Web application lifecycle listener.
 *
 * @author jeca
 */
@WebListener
public class ApplicationContextListener implements ServletContextListener {

    public ApplicationContextListener() {
        System.out.println("=================ApplicationContextListener constructor================");
    }   
    

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("users", initializeUsers());
        sce.getServletContext().setAttribute("departments", new LinkedList<Department>());
        System.out.println("=================users initialized===========================");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("============================context destroyed=============================");
    }

    private List<User> initializeUsers() {
        return new ArrayList(){{
            add(new User("Pera", "Peric", "pera@gmail.com", "pera"));
            add(new User("Zika", "Zikic", "zika@gmail.com", "zika"));
            add(new User("Nikola", "Nikola", "nikola@gmail.com", "nikola"));
            add(new User("Maja", "Majic", "maja@gmail.com", "maja"));   
        }};
    }
}
