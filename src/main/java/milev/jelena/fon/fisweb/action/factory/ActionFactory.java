/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.action.factory;

import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.action.impl.department.ChangeDepartmentAction;
import milev.jelena.fon.fisweb.action.impl.LoginAction;
import milev.jelena.fon.fisweb.action.impl.LogoutAction;
import milev.jelena.fon.fisweb.action.impl.department.AddDepartmentAction;
import milev.jelena.fon.fisweb.action.impl.department.AllDepartmentAction;
import milev.jelena.fon.fisweb.action.impl.department.DeleteDepartmentAction;
import milev.jelena.fon.fisweb.action.impl.department.SaveDepartmentAction;
import milev.jelena.fon.fisweb.action.impl.department.UpdateDepartmentAction;
import milev.jelena.fon.fisweb.constants.ActionConstants;

/**
 *
 * @author jeca
 */
public class ActionFactory {

    public static AbstractAction createActionFactory(String pathInfo) {
        AbstractAction action = null;
        if (pathInfo.equals(ActionConstants.URL_LOGIN)) {
            action = new LoginAction();
        }
        if (pathInfo.equals(ActionConstants.URL_LOGOUT)) {
            action = new LogoutAction();
        }
        if (pathInfo.equals(ActionConstants.URL_ADD_DEPARTMENT)) {
            action = new AddDepartmentAction();
        }
        if (pathInfo.equals(ActionConstants.URL_SAVE_DEPARTMENT)) {
            action = new SaveDepartmentAction();
        }
        if (pathInfo.equals(ActionConstants.URL_ALL_DEPARTMENT)) {
            action = new AllDepartmentAction();
        }
        if (pathInfo.startsWith(ActionConstants.URL_DELETE_DEPARTMENT)) {
            action = new DeleteDepartmentAction();
        }
        if (pathInfo.startsWith(ActionConstants.URL_UPDATE_DEPARTMENT)) {
            action = new UpdateDepartmentAction();
        }
        if (pathInfo.startsWith(ActionConstants.URL_CHANGE_DEPARTMENT)) {
            action = new ChangeDepartmentAction();
        }
       
        return action;
    }
}
