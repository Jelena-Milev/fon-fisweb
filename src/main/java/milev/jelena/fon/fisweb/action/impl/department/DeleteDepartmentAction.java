/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.action.impl.department;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.constants.PageViewConstants;
import milev.jelena.fon.fisweb.model.Department;

/**
 *
 * @author jeca
 */
public class DeleteDepartmentAction extends AbstractAction{

    @Override
    public String execute(HttpServletRequest request) {
        Long id = new Long(request.getParameter("id"));
        List<Department> departments = (List) request.getServletContext().getAttribute("departments");
        for (Department department : departments) {
            if(department.getId().equals(id)){
                departments.remove(department);
                request.setAttribute("messageSuccess", "Department removed successfuly");
                return PageViewConstants.VIEW_ALL_DEPARTMENT;
            }
        }
        request.setAttribute("messageError", "Department does not exist");
        return PageViewConstants.VIEW_ALL_DEPARTMENT;
    }
    
}
