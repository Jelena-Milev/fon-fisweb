/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.action.impl;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.constants.PageViewConstants;
import milev.jelena.fon.fisweb.model.User;

/**
 *
 * @author jeca
 */
public class LoginAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = findUser(request, email, password);
        if(user == null){
            request.setAttribute("message", "Wrong login parameters");
            return PageViewConstants.VIEW_LOGIN;
        }else{
            request.getSession(true).setAttribute("current_user", user);
            return PageViewConstants.VIEW_HOME;
        }
    }

    private User findUser(HttpServletRequest request, String email, String password) {
        List<User> users = (List<User>) request.getServletContext().getAttribute("users");
        User current_user = new User(email, password);
        for (User user : users) {
            if(user.equals(current_user)){
                return user;
            }
        }
        return null;
    }

}
