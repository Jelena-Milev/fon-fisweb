/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.action.impl.department;

import java.util.Comparator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.constants.PageViewConstants;
import milev.jelena.fon.fisweb.model.Department;

/**
 *
 * @author jeca
 */
public class SaveDepartmentAction extends AbstractAction {

    private final Long minId = 1l;

    @Override
    public String execute(HttpServletRequest request) {
        String shortname = request.getParameter("shortname");
        String name = request.getParameter("name");

        Department newDepartment = new Department(shortname, name);
        save(newDepartment, request);
        request.setAttribute("messageAddDepartment", "Department added successfuly.");
        return PageViewConstants.VIEW_ADD_DEPARTMENT;
    }

    private void save(Department newDepartment, HttpServletRequest request) {
        List departments = (List) request.getServletContext().getAttribute("departments");
        setId(departments, newDepartment);
        departments.add(newDepartment);
        System.out.println("==================Departments=====================");
        showDepartments(departments);
    }

    private void setId(List departments, Department newDepartment) {
        if (departments.isEmpty()) {
            newDepartment.setId(minId);
        } else {
            Long maxId = findMaxId(departments);
            newDepartment.setId(maxId + 1l);
        }
    }

    private Long findMaxId(List<Department> departments) {
        Long max = Long.MIN_VALUE;
        for (Department department : departments) {
            if (department.getId() > max) {
                max = department.getId();
            }
        }
        return max;
    }

    private void showDepartments(List<Department> departments) {
        for (Department department : departments) {
            System.out.println(department);
        }
    }
}
