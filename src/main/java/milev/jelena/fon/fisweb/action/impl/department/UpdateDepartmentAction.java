/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.action.impl.department;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.constants.PageViewConstants;
import milev.jelena.fon.fisweb.model.Department;

/**
 *
 * @author jeca
 */
public class UpdateDepartmentAction extends AbstractAction {

    @Override
    public String execute(HttpServletRequest request) {
        Long id = new Long(request.getParameter("id"));
        String name = request.getParameter("name");
        String shortname = request.getParameter("shortname");

        if ((name!=null && name.isEmpty()) || (shortname!=null && shortname.isEmpty())) {
            request.setAttribute("messageError", "Name and shortname can not be empty.");
            return PageViewConstants.VIEW_ALL_DEPARTMENT;
        }

        Department newDepartment = new Department(id, shortname, name);
        return updateDepartment(newDepartment, request);
    }

    private String updateDepartment(Department newDepartment, HttpServletRequest request) {
        List<Department> departments = (List<Department>) request.getServletContext().getAttribute("departments");
        for (Department department : departments) {
            if (department.getId().equals(newDepartment.getId())) {
                department.setName(newDepartment.getName());
                department.setShortname(newDepartment.getShortname());
                request.setAttribute("messageSuccess", "Department changed successfuly.");
                return PageViewConstants.VIEW_ALL_DEPARTMENT;
            }
        }
        request.setAttribute("messageError", "Department does not exist.");
        return PageViewConstants.VIEW_ALL_DEPARTMENT;
    }
}
