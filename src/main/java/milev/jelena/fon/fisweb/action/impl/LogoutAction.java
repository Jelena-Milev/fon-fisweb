/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.action.impl;

import javax.servlet.http.HttpServletRequest;
import milev.jelena.fon.fisweb.action.AbstractAction;
import milev.jelena.fon.fisweb.constants.PageViewConstants;

/**
 *
 * @author jeca
 */
public class LogoutAction extends AbstractAction{

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().removeAttribute("current_user");
        return PageViewConstants.VIEW_LOGIN;
    }
    
}
