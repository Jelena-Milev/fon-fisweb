/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milev.jelena.fon.fisweb.constants;

/**
 *
 * @author jeca
 */
public interface PageViewConstants {
    
    public static final String PAGE_LOGIN = "/login.jsp";
    public static final String PAGE_HOME = "/WEB-INF/pages/home.jsp";
    public static final String PAGE_DEFAULT_ERROR = "/WEB-INF/pages/error/default_error.jsp";
    public static final String PAGE_ADD_DEPARTMENT = "/WEB-INF/pages/department/add.jsp";
    public static final String PAGE_ALL_DEPARTMENT = "/WEB-INF/pages/department/all.jsp";
    public static final String PAGE_UPDATE_DEPARTMENT = "/WEB-INF/pages/department/update.jsp";
    
    
    public static final String VIEW_LOGIN = "VIEW_LOGIN";
    public static final String VIEW_HOME = "VIEW_HOME";
    public static final String VIEW_DEFAULT_ERROR = "VIEW_DEFAULT_ERROR";
    public static final String VIEW_ADD_DEPARTMENT = "VIEW_ADD_DEPARTMENT";
    public static final String VIEW_ALL_DEPARTMENT = "VIEW_ALL_DEPARTMENT";
    public static final String VIEW_UPDATE_DEPARTMENT = "VIEW_UPDATE_DEPARTMENT";
    
}
